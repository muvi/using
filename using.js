/////////////////////////////////////////////////////////////////////////////////////////////
//
// using.js
// v2.10.2
//
//    A cross-platform, expandable module loader for javascript.
//
// License
//    Apache License Version 2.0
//
// Copyright Nick Verlinden
//
/////////////////////////////////////////////////////////////////////////////////////////////

var define;
var using;
(function() {
    /////////////////////////////////////////////////////////////////////////////////////////
    //
    // verify environment support
    //
    /////////////////////////////////////////////////////////////////////////////////////////
    var errMiss = "using.js: error-missing-feature. The '";
    var errMissTail = "' feature is not supported by the runtime.";
    if (!Function || !Function.prototype || !Function.prototype.bind) {
        if (typeof console != "undefined" && console.error) {
            console.error(errMiss + "Function.prototype.bind" + errMissTail);
            return;
        }
        else {
            throw errMiss + "Function.prototype.bind" + errMissTail;
        }
    }
    if (!Array || !Array.prototype || !Array.prototype.map) {
        if (typeof console != "undefined" && console.error) {
            console.error(errMiss + "Array.prototype.map" + errMissTail);
            return;
        }
        else {
            throw errMiss + "Array.prototype.map" + errMissTail;
        }
    }
    if (!Object || !Object.defineProperty) {
        if (typeof console != "undefined" && console.error) {
            console.error(errMiss + "Object.defineProperty" + errMissTail);
            return;
        }
        else {
            throw errMiss + "Object.defineProperty" + errMissTail;
        }
    }

    /////////////////////////////////////////////////////////////////////////////////////////
    //
    // Module class
    //
    /////////////////////////////////////////////////////////////////////////////////////////
    function Module() {
        var self = this;

        this.id = null;
        this.dependencies = [];
        this.define = null;
        this.factory = null;
        this.parameters = null;

        // get a dependency by it's id (wildcard * allowed, takes highest alphanumeric match, keeps dots and slashes into account)
        this.dependencies.get = function(id, opt_upgradable, request, requirer) {
            if (!self.dependencies || Object.prototype.toString.call(self.dependencies) !== "[object Array]") {
                return;
            }
            if (request && Object.prototype.toString.call(request) !== "[object Object]") {
                throw new Error(using.ERROR_INVALID_REQUEST, "Optional parameter 'request' should be an object of type 'Object'.");
            }

            // check directly
            if (self.dependencies[id] && self.dependencies[id] instanceof Module) {
                return self.dependencies[id].factory(request, requirer);
            }

            // compare by search string
            var dependencies;
            if (!opt_upgradable) {
                dependencies = [];
                for (var d in self.dependencies) {
                    if (self.dependencies[d] instanceof Module) {
                        dependencies[self.dependencies[d].id] = self.dependencies[d];
                    }
                }
            }

            var sorted = sortById(dependencies? dependencies : cache, "desc");
            for (var d in sorted) {
                if (!isNaN(d) && sorted[d] instanceof Module && compareId(sorted[d].id, id, opt_upgradable? opt_upgradable : using.UPGRADABLE_NONE)) {
                    return typeof sorted[d].factory === "function" ? sorted[d].factory(request, requirer) : sorted[d].factory;
                }
            }

            throw new Error(using.ERROR_DEPENDENCY_NOT_FOUND, "Dependency '" + id + "' could not be found for module '" + self.id + "'.", self);
        };
    }

    /////////////////////////////////////////////////////////////////////////////////////////
    //
    // Loader class
    //
    /////////////////////////////////////////////////////////////////////////////////////////
    function Loader(request, loader) {
        var self = this;

        var done = false;
        var progress = 0;

        this.err = [];
        this.request = request;
        this.module = null;
        Object.defineProperty(self, "progress", {
            get: function() {
                return progress;
            },
            set: function(value) {
                progress = value;
                self.events.fire(using.EVENT_REQUEST_PROGRESS, progress);
            }
        });
        Object.defineProperty(self, "state", {
            get: function() {
                return ((self.module || done) && self.err.length == 0)? using.STATE_SUCCESS : (done? using.STATE_ERROR : using.STATE_INITIAL);
            }
        });

        this.events = new Emitter(this);

        // bind the fetch function to the loader function
        this.fetch = function(callback) {
            try {
                function moduleLoaded(module, gracefull) {
                    if (done) {
                        self.err.push(new Error(using.ERROR_UNEXPECTED, "The Loader already finished, but tried to invoke the fetch callback again."));
                        return;
                    }
                    done = true;

                    if (module instanceof Module) {
                        self.module = module;

                        if (module.parameters.callFactoryAfterFetch) {
                            module.factory();
                        }

                        self.events.fire(using.EVENT_REQUEST_SUCCESS);
                    }
                    else if (module) {
                        self.err.push(new Error(using.ERROR_UNEXPECTED, "An unexpected error occurred in the Loader while trying to fetch the request '" + JSON.stringify(request) + "'. The Loader did not return an object of type 'Module'."));
                        self.events.fire(using.EVENT_REQUEST_ERROR);
                    }
                    else if (self.err.length > 0 && !gracefull) {
                        self.events.fire(using.EVENT_REQUEST_ERROR);
                    }
                    else {
                        self.events.fire(using.EVENT_REQUEST_SUCCESS);
                    }

                    if (callback) {
                        if (Object.prototype.toString.call(callback) !== "[object Function]") {
                            self.err.push(new Error(using.ERROR_UNEXPECTED, "Optional parameter 'callback' should be an object of type 'Function'."));
                            self.events.fire(using.EVENT_REQUEST_ERROR);
                        }
                        else {
                            callback();
                        }
                    }
                }

                // check cache
                if (!request.ignoreCache) {
                    var sorted = sortById(cache, "desc");
                    for (var d in sorted) {
                        if (!isNaN(d) && sorted[d] instanceof Module && compareId(sorted[d].id, request.id, using.UPGRADABLE_NONE)) {
                            moduleLoaded(sorted[d]);
                            break;
                        }
                    }
                }
                
                if (!self.module) {
                    // invoke loader
                    loader(moduleLoaded);
                }
            }
            catch(e) {
                done = true;

                self.err.push(new Error(using.ERROR_UNEXPECTED, "An unexpected error occurred in the Loader while trying to fetch the request '" + JSON.stringify(request) + "'.", e));
                self.events.fire(using.EVENT_REQUEST_ERROR);
            }
        };
    }

    /////////////////////////////////////////////////////////////////////////////////////////
    //
    // Using class
    //
    /////////////////////////////////////////////////////////////////////////////////////////
    function Using() {
        var self = this;

        var progress = 0;

        this.context = null;
        this.requests = [];
        this.err = [];
        Object.defineProperty(self, "progress", {
            get: function() {
                return progress;
            }
        });

        this.events = new Emitter(this);

        this.then = function(success, fail, bypassFactory) {
            var successClbk = success;
            var failClbk = fail;
            fail = function() {
                self.events.fire(using.EVENT_USING_ERROR);
                if (failClbk) {
                    failClbk.apply(failClbk, arguments);
                }
            };
            success = function() {
                self.events.fire(using.EVENT_USING_SUCCESS);
                if (successClbk) {
                    successClbk.apply(successClbk, arguments);
                }
            };

            // check for initialisation errors and empty request
            if (self.err.length > 0) {
                fail(self);
                return;
            }
            else if (self.requests.length == 0) {
                success();
                return;
            }

            // enumerate loaders
            for (var l=0;l<self.requests.length;l++) {
                self.requests[l].on([ using.EVENT_REQUEST_SUCCESS, using.EVENT_REQUEST_ERROR ], function(loader) {
                    var allDone = true;
                    var error = false;

                    // check state of all loaders
                    for (var r = 0; r < self.requests.length; r++) {
                        if (self.requests[r].state != using.STATE_SUCCESS &&
                            self.requests[r].state != using.STATE_ERROR) {
                            allDone = false;
                            break;
                        }
                        if (self.requests[r].state == using.STATE_ERROR) {
                            error = true;
                        }
                    }

                    if (allDone) {
                        if (error) {
                            fail(self);
                        }
                        else {
                            var results = [];
                            for (var r = 0; r < self.requests.length; r++) {
                                if (self.requests[r].module instanceof Module) {
                                    results[r] = self.requests[r].module;
                                    try {
                                        results[r].instance = bypassFactory? self.requests[r].module : (Object.prototype.toString.call(self.requests[r].module.factory) !== "[object Function]"? self.requests[r].module.factory : self.requests[r].module.factory(self.requests[r].request, self.context) );
                                    }
                                    catch (e) {
                                        self.err.push(new Error(using.ERROR_MODULE, e));
                                        fail(self);
                                        return;
                                    }
                                }
                                else {
                                    self.err.push(new Error(using.ERROR_UNEXPECTED, "Mandatory property 'module' should be an object of type 'Module'. An object of the '" + Object.prototype.toString.call(self.requests[r].module) + "' was provided."));
                                    fail(self);
                                    return;
                                }
                            }
                            success.apply(success, results);
                        }
                    }
                });
                self.requests[l].on(using.EVENT_REQUEST_PROGRESS, function(loader) {
                    // sum of all request progress
                    var p = 0;
                    for (var r = 0; r < self.requests.length; r++) {
                        p += self.requests[l].progress;
                    }
                    progress = p / self.requests.length;
                    self.events.fire(self.EVENT_USING_PROGRESS, progress);
                });
                try {
                    using.events.fire(using.EVENT_REQUEST, self.requests[l]);
                    self.requests[l].fetch();
                }
                catch(e) {
                    self.err.push(new Error(using.ERROR_UNEXPECTED, "The loader for request '" + JSON.stringify(self.requests[l].request) + "' has thrown an unexpected error.", e));
                    fail(self);
                }
            }
        };

        // fire new request event
        using.events.fire(using.EVENT_USING);
    }

    /////////////////////////////////////////////////////////////////////////////////////////
    //
    // Emitter class
    //
    /////////////////////////////////////////////////////////////////////////////////////////
    function Emitter(context) {
        var own = this;
        var callbacks = [];

        this.on = function (type, callback) {
            if (Object.prototype.toString.call(type) === "[object Array]") {
                for (var t in type) {
                    own.on(type[t], callback);
                }
                return;
            }
            var clb = { "callback" : callback, "type" : type };
            clb.id = callbacks.push(clb) - 1;

            return clb.id;
        };
        this.addEventListener = this.on;
        if (!context.addEventListener) {
            context.addEventListener = this.on;
        }
        if (!context.on) {
            context.on = this.on;
        }

        this.removeEventListener = function (id) {
            if (Object.prototype.toString.call(id) === "[object Function]") {
                for (var i in callbacks) {
                    if (callbacks[i].callback == id) {
                        callbacks.splice(i, 1);
                        i--;
                    }
                }
            }
            else {
                callbacks.splice(id, 1);
            }
        };
        if (!context.removeEventListener) {
            context.removeEventListener = this.removeEventListener;
        }

        this.fire = function (type, opt_arg) {
            for (var e = 0; e < callbacks.length; e++) {
                if (callbacks[e] != null && (callbacks[e].type == type || callbacks[e].type == "*")) {
                    try {
                        var a = [ context || global || window ];
                        for (var i in arguments) {
                            if (i == 0) {
                                continue;
                            }
                            a.push(arguments[i]);
                        }
                        if (callbacks[e].type == "*") {
                            a.unshift(type);
                        }
                        var retVal = callbacks[e].callback.apply(context, a);
                        if (retVal == true) {
                            return true;
                        }
                    }
                    catch(ex) {
                        if (Object.prototype.toString.call(callbacks[e].callback) !== "[object Function]") {
                            context.removeEventListener(e);
                            e--;
                        }
                    }
                }
            }
        };
    }

    /////////////////////////////////////////////////////////////////////////////////////////
    //
    // define function
    //
    /////////////////////////////////////////////////////////////////////////////////////////
    // in-memory cache that contains factories for javascript resources
    var cache = [];
    var cacheCounter = 0;
    var cacheCallbacks = {};

    define = function(/* optional */ id, /* optional */ dependencies, factory) {
        var errSrc = "define(id, dependencies, factory): Invalid module definition. ";

        // set correct parameters, from last to first
        if (Object.prototype.toString.call(dependencies) === "[object Function]") {
            factory = dependencies;
            dependencies = null;
        }
        if (Object.prototype.toString.call(id) === "[object Function]") {
            factory = id;
            id = null;
        }
        if (Object.prototype.toString.call(id) === "[object Array]") {
            dependencies = id;
            id = null;
        }

        var mod = createOrUpdateModule(id, dependencies, factory);

        // module factory, this is where default dependencies are injected
        var f = function() {
            // check for missing dependencies
            for (var d in arguments) {
                if (!isNaN(d) && Object.prototype.toString.call(arguments[d]) === "[object String]") {
                    // check if loader for module system could be found. It will throw an error if the module loader is not found.
                    define.Loader.get(mod.system);

                    // if the loader was found, this means a dependency could not be resolved.
                    throw new RangeError(errSrc + "Dependency '" + arguments[d] + "' could not be resolved.");
                    //arguments[(parseInt(d)+1)] = null;
                }
            }

            return factory.apply(factory, arguments);
        };
        mod.factory = mod.system? function(request, requirer) { 
            var l;
            try {
                l = define.Loader.get(mod.system);
            }
            catch(e) {
                // ignore
            }
            return (l? l.factory : f)(mod, f, request, requirer);
        } : f;

        // if the parameter is set, the factory will be called right after define.
        if (mod.parameters.callFactoryAfterDefine) {
            mod.factory();
        }

        // check cache callbacks
        for (var i in cacheCallbacks) {
            if (compareId(mod.id, i) || i == mod.id) {
                while (cacheCallbacks[id] && cacheCallbacks[id].length > 0) {
                    try {
                        cacheCallbacks[id][0](mod);
                    }
                    catch(e) {
                        if (typeof console != "undefined") {
                            console.error("An error occurred inside a cache registered callback for id '" + i + "'.", e);
                        }
                    }
                    cacheCallbacks[id].splice(0,1);
                }
            }
        }
    };
    define.prepare = function() {
        createOrUpdateModule.apply(arguments);
    };
    function createOrUpdateModule(id, dependencies, factory) {
        var errSrc = "define(id, dependencies, factory): Invalid module definition. ";
        var system;

        // override parameters if set
        if (define.parameters) {
            if (Object.prototype.toString.call(define.parameters) === "[object Object]") {
                parameters = define.parameters;
                // override default ones
                id = id || parameters.id;
                dependencies = dependencies || parameters.dependencies;
                factory = factory || parameters.factory;
                system = parameters.system || system; 
            }
            else {
                throw new TypeError(errSrc + "Optional define.parameters should be an object of type 'Object'.");
            }
        }

        // verify parameter type
        if (Object.prototype.toString.call(id) !== "[object String]" && id != null) {
            throw new TypeError(errSrc + "Optional parameter 'id' should be an object of type 'String'.");
        }
        if (dependencies != null) {
            if (Object.prototype.toString.call(dependencies) !== "[object Array]") {
                throw new TypeError(errSrc + "Optional parameter 'dependencies' should be an object of type 'Array'.");
            }
            else {
                for (var i=0;i<dependencies.length;i++) {
                    if (Object.prototype.toString.call(dependencies[i]) !== "[object String]" && Object.prototype.toString.call(dependencies[i]) !== "[object Object]") {
                        throw new TypeError(errSrc + "Optional parameter 'dependencies' should be an array of objects of type 'String'. Got: "+ Object.prototype.toString.call(dependencies[i]));
                    }
                }
            }
        }

        if (Object.prototype.toString.call(system) !== "[object String]" && system) {
            throw new TypeError(errSrc + "Optional parameter 'system' should be an object of type 'String'.");
        }

        // add default dependencies if not set
        if (!dependencies) {
            dependencies = [];
        }
        if (dependencies.length == 0) {
            dependencies.push(using.DEPENDENCY_MODULE);
        }

        // create module
        var mod = cache[id] || new Module();
        if (cache[id]) {
            while(mod.dependencies.length) {
                mod.dependencies.splice(0,1);
            }
        }
        mod.id = id;
        mod.parameters = parameters;
        mod.factory = factory;
        mod.system = system;

        // decorate dependencies
        for (var d in dependencies) {
            if (!isNaN(d) && !cache[id]) {
                switch (dependencies[d]) {
                    case using.DEPENDENCY_MODULE:
                        dependencies[d] = mod;
                        break;
                }
                mod.dependencies.push(dependencies[d]);
            }
            else {
                mod.dependencies[d] = dependencies[d];
            }
        }

        // create definition and add to memory cache
        var cId = id || "mod-" + cacheCounter++;
        cache[cId] = mod;

        return mod;
    }

    // parameters for overriding default function parameters when calling define, or for
    // adding custom parameters that the loader can read
    define.parameters = {};

    // in-memory cache functions for finding modules
    define.cache = {};
    // get a module by it's id (wildcard * allowed, takes highest alphanumeric match, takes
    // dots and slashes into account)
    define.cache.get = function (id, opt_upgradable) {
        if (!id) {
            var last;
            for (var i in cache) {
                last = cache[i];
            }
            return last;
        }

        var cacheSorted = sortById(cache, id == "*"? "asc" : "desc");
        
        if (id == "*") {
            return cacheSorted;    
        }

        for (var m in cacheSorted) {
            if (compareId(cacheSorted[m].id, id, opt_upgradable)) {
                return cacheSorted[m];
            }
        }
    };
    define.cache.waitFor = function(id, callback) {
        var mod = define.cache.get(id);
        if (mod) {
            callback(mod);
            return;
        }

        cacheCallbacks[id] = cacheCallbacks[id] || [];
        cacheCallbacks[id].push(callback);
    };

    // loaders that process define requests
    var loaders = {};
    var loaderCallbacks = {};
    define.Loader = Loader;
    define.Loader.register = function(system, fn) {
        var errSrc = "define.loaders.register(system, fn): ";
        if (Object.prototype.toString.call(system) !== "[object String]") {
            throw new TypeError(errSrc + "Invalid loader. Mandatory parameter 'system' should be an object of type 'String'.");
        }
        if (Object.prototype.toString.call(fn) !== "[object Function]") {
            throw new TypeError(errSrc + "Invalid loader. Mandatory parameter 'fn' should be an object of type 'Function'.");
        }

        for (var l in loaders) {
            if (loaders[l] == fn) {
                throw new RangeError(errSrc + "Loader already registered.");
            }
        }

        if (loaders[system]) {
            throw new RangeError(errSrc + "A loader for module system '" + system + "' is already registered.");
        }

        loaders[system] = fn;

        while (loaderCallbacks[system] && loaderCallbacks[system].length > 0) {
            try {
                loaderCallbacks[system][0](fn);
            }
            catch(e) {
                if (typeof console != "undefined") {
                    console.error("An error occurred inside a loader registered callback for system '" + system + "'.", e);
                }
            }
            loaderCallbacks[system].splice(0,1);
        }
    };
    define.Loader.get = function(system) {
        var errSrc = "define.loaders.get(system): Invalid loader. ";
        if (Object.prototype.toString.call(system) !== "[object String]") {
            throw new TypeError(errSrc + "Mandatory parameter 'system' should be an object of type 'String'.");
        }
        if (!loaders[system]) {
            throw new RangeError(errSrc + "No loader was found for module system '" + system + "'.");
        }

        return loaders[system];
    };
    define.Loader.waitFor = function(system, callback) {
        if (loaders[system]) {
            callback(loaders[system]);
            return;
        }

        loaderCallbacks[system] = loaderCallbacks[system] || [];
        loaderCallbacks[system].push(callback);
    };

    // module definition object
    define.Module = Module;

    // require files in the context of the given id
    var requireCache = [];
    var nativePaths = [];
    var nodePath = typeof require === "function" && require.main? require("path") : null;
    define.addNativeRequirePath = function(path) {
        if (!nativePaths.indexOf(path) > -1) {
            return;
        }

        nativePaths.push(path);
    };
    define.getRequire = function(moduleId, original) {
        function addPathToOriginal(require, path) {
            if (require.original) {
                return addPathToOriginal(require.original);
            }
    
            if (require.main && require.main.paths && require.main.paths.indexOf(path) === -1) {
                require.main.paths.push(path);
                require.main.paths.push(path + (nodePath? nodePath.sep : "/") + moduleId);
                require.main.paths.push(path + (nodePath? nodePath.sep : "/") + moduleId + (nodePath? nodePath.sep : "/") + "node_modules");
            }
        }

        return function(id, opt_upgradable) {
            this.original = original;
            if (!moduleId || !cache[moduleId]) {
                if (typeof require === "function") {
                    if (!requireCache[id]) {
                        requireCache[id] = require(id);
                    }
                    return requireCache[id];
                }
                return function () {
                    throw new Error("Runtime environment does not implement 'require'.");
                };
            }
            
            // try using cache first
            var err;
            try {
                var result;
                var idS = id;
                if (idS.substr(0,2) == "./") {
                    idS = idS.substr(2);
                }
                idS += idS.indexOf("/", 0) == -1? "/" : "";
                result = cache[moduleId].dependencies.get(idS, opt_upgradable, null, cache[moduleId]);
            }
            catch(e) {
                if (e.name != using.ERROR_DEPENDENCY_NOT_FOUND) {
                    throw e;
                }
                err = e;
            }

            // then try original
            if (!result && original) {
                // check if native paths are known to the original
                for (var p in nativePaths) {
                    addPathToOriginal(original, nativePaths[p]);
                }

                try {
                    result = original(id);
                }
                catch(e) {
                    if (e.code !== "MODULE_NOT_FOUND") {
                        throw e;
                    }
                    // parse get string
                    if (id.length > 2 && id.substr(0,2) == "./") {
                        id = id.substr(2);

                        try {
                            result = original(id);
                        }
                        catch(e) {
                            if (e.code !== "MODULE_NOT_FOUND") {
                                throw e;
                            }
                            // the original could not resolve
                            result = null;
                        }
                    }
                }

                if (!result && err) {
                    throw err;
                }
            }

            return result;
        };
    };
    define.getUsing = function(moduleId) {
        var usingInContext = function() {
            var args = [];
            for (var a in arguments) {
                args.push(arguments[a]);
            }
            if (cache[moduleId]) {
                args.push(cache[moduleId]);
            }
            return using.apply(this, args);
        };
        usingInContext.original = using;
        return usingInContext;
    };
    define.Module = Module;

    // this property can be used to check if define is the using.js variant instead of the
    // AMD variant
    define.using = true;

    /////////////////////////////////////////////////////////////////////////////////////////
    //
    // using function
    //
    /////////////////////////////////////////////////////////////////////////////////////////
    using = function() {
        // create handler
        var handler = new Using();

        if (arguments[arguments.length - 1] instanceof Module) {
            handler.context = arguments[arguments.length - 1];
        }

        // enumerate requests
        for(var a=0;a<arguments.length - (handler.context? 1 : 0);a++) {
            var request = arguments[a];
            var match = false;

            // check request object validity
            if (Object.prototype.toString.call(arguments[a]) !== "[object Object]" &&
                Object.prototype.toString.call(arguments[a]) !== "[object String]") {
                throw new TypeError("using(): Each of the arguments passes should be an object of type 'Object' or an object of type 'String'.");
            }

            // enumerate an try the loaders one by one in order of registration for this request
            for (var l in loaders) {
                var loader = null;
                try {
                    loader = new loaders[l](request, handler);
                } catch(e) {
                    if (!(e instanceof RangeError)) {
                        handler.err.push(new Error(using.ERROR_UNEXPECTED, "The loader for request '" + JSON.stringify(request) + "' has thrown an unexpected error.", e, request));
                    }
                    else {
                        handler.err.push(new Error(using.WARNING_REQUEST_NOT_ACCEPTED, "Loader '" + l + "' did not accept request '" + JSON.stringify(request) + "'.", e, request));
                    }
                    // try other loaders
                    continue;
                }

                if (loader instanceof Loader) {
                    handler.requests.push(loader);
                    match = true;
                    break;
                }
                else {
                    handler.err.push(new Error(using.ERROR_UNEXPECTED, "The loader did not return an object of type 'Loader'.", loaders[l]));
                }
            }

            if (!match) {
                handler.err.push(new Error(using.ERROR_UNSUPPORTED_REQUEST, "Unsupported request '" + JSON.stringify(request) + "', no loader was found.", request));
            }
        }

        return handler;
    };
    // create static event emitter
    using.events = new Emitter(using);

    /////////////////////////////////////////////////////////////////////////////////////////
    //
    // constants
    //
    /////////////////////////////////////////////////////////////////////////////////////////
    using.EVENT_USING = "event-using";
    using.EVENT_USING_PROGRESS = "event-using-progress";
    using.EVENT_USING_SUCCESS = "event-using-success";
    using.EVENT_USING_ERROR = "event-using-error";
    using.EVENT_REQUEST = "event-request";
    using.EVENT_REQUEST_PROGRESS = "event-request-progress";
    using.EVENT_REQUEST_SUCCESS = "event-request-success";
    using.EVENT_REQUEST_ERROR = "event-request-error";
    using.EVENT_PACKAGE_MOUNT_ERROR = "event-package-mount-error";
    using.EVENT_PACKAGE_MOUNT_SUCCESS = "event-package-mount-success";
    using.STATE_INITIAL = "state-initial";
    using.STATE_SUCCESS = "state-success";
    using.STATE_ERROR = "state-error";
    using.DEPENDENCY_MODULE = "module";
    using.UPGRADABLE_NONE = null;
    using.UPGRADABLE_PATCH = "patch";
    using.UPGRADABLE_MINOR = "minor";
    using.UPGRADABLE_MAJOR = "major";
    using.ERROR_PACKAGE = "Package Error";
    using.ERROR_INVALID_REQUEST = "Invalid Request";
    using.ERROR_UNSUPPORTED_REQUEST = "Unsupported Request";
    using.ERROR_MODULE = "Module Error";
    using.ERROR_UNEXPECTED = "Unexpected Error";
    using.ERROR_DEPENDENCY_NOT_FOUND = "Missing Dependency";
    using.WARNING_REQUEST_NOT_ACCEPTED = "Request Not Accepted";
    define.ERROR_INVALID_REQUEST_HANDLER = "Invalid Request Handler";

    /////////////////////////////////////////////////////////////////////////////////////////
    //
    // utility functions
    //
    /////////////////////////////////////////////////////////////////////////////////////////
    function sortById(arr, order) {
        var props = [];
        for (var p in arr) {
            props.push(p);
        }

        var sort = {
            asc: function (a, b) {
                var reA = /[^a-zA-Z]/g;
                var reN = /[^0-9]/g;
                for (var l=0;l < Math.min(a.value.length, b.value.length); l++) {
                    if (a.value[l] === b.value[l]) {
                        continue;
                    }
                    // put numbers before strings
                    if (!isNaN(a.value[l]) && isNaN(b.value[l])) {
                        return 1;
                    }
                    if (isNaN(a.value[l]) && !isNaN(b.value[l])) {
                        return -1;
                    }
                    // compare
                    var aA = a.value[l].replace(reA, "");
                    var bA = b.value[l].replace(reA, "");
                    if(aA === bA) {
                        var aN = parseInt(a.value[l].replace(reN, ""), 10);
                        var bN = parseInt(b.value[l].replace(reN, ""), 10);
                        //return aN === bN ? 0 : aN > bN ? 1 : -1;
                        if (aN === bN) {
                            continue;
                        }
                        return aN > bN ? 1 : -1;
                    } else {
                        return aA > bA ? 1 : -1;
                    }
                }
                return 0;
            },
            desc: function (a, b) {
                return sort.asc(b, a);
            }
        };

        var mapped = props.map(function (el, i) {
            return { index: i, value: el.split(/[\.\/]+/), name : el };
        });

        mapped.sort(sort[order] || sort.asc);

        return mapped.map(function (el) {
            return arr[el.name];
        });
    }

    function compareId(str, search, opt_upgradable) {
        if (!str || !search) {
            return;
        }
        
        var strRes = str.split("/");
        str = strRes[0];
        if (strRes.length == 1) {
            strRes[1] = "";
        }
        strRes = strRes.join("/").substr(str.length);
        var searchRes = search.split("/");
        search = searchRes[0];
        if (searchRes.length == 1) {
            searchRes[1] = "";
        }
        searchRes = searchRes.join("/").substr(search.length);

        var parts = search.split(".");
        var numbers = 0;
        var firstNonZero;
        var carret;
        for (var p = parts.length; p > 0; p--) {
            if (parts[p -1 ].substr(0,1) == "^") {
                carret = true;
                parts[p -1 ] = parts[p -1 ].substr(1);
            };
            if (isNaN(parts[p - 1])) {
                break;
            }
            if (parts[p - 1] !== 0 && firstNonZero == null) {
                firstNonZero = numbers;
            }
            numbers++;
        }

        // carret
        if (carret) {
            if (!firstNonZero) {
                firstNonZero = 2;
            }

            switch(2 - firstNonZero) {
                case 0:
                    opt_upgradable = using.UPGRADABLE_MINOR;
                    break;
                case 1:
                    opt_upgradable = using.UPGRADABLE_PATCH;
                    break;
            }
        }

        var groups = 0;
        switch (opt_upgradable) {
            case using.UPGRADABLE_PATCH:
                groups = 1;
                break;
            case using.UPGRADABLE_MINOR:
                groups = 2;
                break;
            case using.UPGRADABLE_MAJOR:
                groups = 3;
                break;
        }
        //if (groups === 0) {
        //    return str == search;
        //}

        var match = str.split(".");
        // if parts are not equal, they can't match
        if (match.length != (parts.length - numbers) + 3 || strRes != searchRes) {
            return false;
        }
        // check if id string without number parts match
        for (var p = 0; p < parts.length - numbers; p++) {
            if (match.length < p || match[p] != parts[p]) {
                return false;
            }
        }
        // check the number parts
        for (var g = 0; g < 3; g++) {
            if (g < 3 - numbers) {
                continue;
            }
            if (g < groups && match[match.length - 1 - g] < parts[parts.length - 1 - (g - (3 - numbers))]) {
                return false;
            }
            else if(g >= groups && match[match.length - 1 - g] != parts[parts.length - 1 - (g - (3 - numbers))]) {
                return false;
            }
        }

        return true;
    }

    /////////////////////////////////////////////////////////////////////////////////////////
    //
    // post-initialisation
    //
    /////////////////////////////////////////////////////////////////////////////////////////
    // add to global scope
    if (typeof global != "undefined") {
        global.using = using;
        global.define = define;
    }
})();

if (typeof require === "function" && require.main) {
    module.exports = using;
}